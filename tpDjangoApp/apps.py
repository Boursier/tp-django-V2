from django.apps import AppConfig


class TpdjangoappConfig(AppConfig):
    name = 'tpDjangoApp'
