from django.core.mail import EmailMessage

def mail(subject, body, mail_from, mail_to, attachements=None):
	mail = EmailMessage(
		subject,
		body,
		mail_from,
		mail_to
	)
	if attachements is not None:
		for attachement in attachements:
			mail.attach(
				attachement[0], # File name
				attachement[1], # File content
				attachement[2]	# MIME type
			)
	mail.send()
