import csv
import io
from . import licenceDict

def createCSV(request):
	dic = licenceDict.licenceInfo(request)
	csvfile = io.StringIO()
	writer = csv.DictWriter(csvfile, dic.keys())
	writer.writeheader()
	data = {key: value for key, value in dic.items()
		if key in dic.keys()}
	writer.writerow(data)
	return csvfile.getvalue()
