from collections import OrderedDict
from datetime import datetime

def licenceInfo(request):
	return OrderedDict([
		('Nom', request.POST.get('name')),
		('Prenom', request.POST.get('firstname')),
		('Email', request.POST.get('email')),
		('Telephone', request.POST.get('telephone')),
		('Bureau', request.POST.get('bureau')),
		('Batiment', request.POST.get('batiment')),
		('MatlabRelease', request.POST.get('matlabRelease')),
		('OperatingSystem', request.POST.get('operatingSystem')),
		('HostID', request.POST.get('hostID')),
	])
	
