from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.formulaire_renouvelement, name='formulaire_renouvelement')
]