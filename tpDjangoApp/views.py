from django.shortcuts import render, redirect
from django.conf import settings
from .forms import LicenceForm
from .helpers import mail, csv

def formulaire_renouvelement(request):
	if request.method == "POST":
		form = LicenceForm(request.POST)
		if form.is_valid():
			firstname = request.POST.get('firstname')
			name = request.POST.get('name')

			# Creation des pièces jointes
			attachements = [
				(' '.join([firstname, name]) + '.csv', csv.createCSV(request), 'text/csv' ),
				]
			
			# Creation et envoi du mail pour le geeps
			mail.mail(
				'Nouveau visiteur',
				'Les informations sur le visiteur sont dans les fichiers pdf et csv',
				getattr(settings, "EMAIL_HOST_USER", None),
				[getattr(settings, "EMAIL_HOST_RECEIVER", None)],
				attachements
				
			)

			form.save()
			return render(request, 'tpDjangoApp/formulaire_renouvelement.html', {'form': form})
	else:
		form = LicenceForm()
	return render(request, 'tpDjangoApp/formulaire_renouvelement.html', {'form': form})
