from django import forms

from .models import Licence

class LicenceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for _, value in self.fields.items():
            value.widget.attrs['placeholder'] = value.help_text

    class Meta:
        model = Licence
        fields = ('name', 'firstname', 'email', 'telephone', 'bureau', 'batiment', 'matlabRelease', 'operatingSystem', 'hostID', )



   
