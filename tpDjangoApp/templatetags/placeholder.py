from django.template import Library


register = Library()
 
@register.filter(name='placeholder')
def placeholder(field, placeholder):
    return field.as_widget(attrs={'placeholder': placeholder})


