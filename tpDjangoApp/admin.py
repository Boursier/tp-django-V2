from django.contrib import admin
from .models import Licence

# Register your models here.
admin.site.register(Licence)